# Random Image Generation NFT

### License:
MIT

## FAQ:
### How do I run this?
To run the entire traits and image generation at once, run the following from the project folder:

`python generate_nft_metadata_and_images.py`

### What about my metadata? Why is it in two places?
Metadata will generate in two forms: 

First, in the same format Boring Bananas Co originally used, in a traits_only.json file.

Second, the script will also automatically create IPFS-ready data in the metadata_format folder. This data is formatted for Open Sea, the only thing left to add will be the image IPFS CIDs after generation.

### This is a pain in the ass to add items, I have to edit multiple lists!
Yes, I'm going to update this to take in requirements (Names, image names, generation odds, etc.) from a single file soon. Stay tuned.
