from PIL import Image
from concurrent.futures import ProcessPoolExecutor

# All files defined here MUST exist in the image_generation/image_data/<FILETYPE HERE>/<FILENAME HERE> in
# order to generate
# Example: Background file for "White" below corresponds to bg001, which would
# be located at: $PROJECT_DIR/image_generation/image_data/backgrounds/bg001.png
#
# Please make sure to have all of your images properly named and in place before running the script
# or you will get errors

backgroundfiles = {
    "White": "bg001",
    "Purple": "bg002",
    "Black": "bg003"
}

bodyfiles = {
    "Big Chest": "body001",
    "Metal Plates": "body002",
    "Pancakes": "body003"
}

mouthfiles = {

    "No Teeth": "m001",
    "Smile": "m002",
    "Sad": "m003"
}

eyefiles = {
    "Surprised": "eye001",
    "Happy": "eye002",
    "Squint": "eye003"
}

heads = {
    "Normal": "a000_f",
    "Other": "a001_f",
    "Test": "a002_f"
}

head_backs = {
    "Normal": "a000_b",
    "Other": "a001_b",
    "Test": "a002_b"
}

accessories = {
    "None": "a000_f",
    "Necklace": "a007_f",
    "Vest": "a010_f"
}

accessories_backs = {
    "None": "a000_b",
    "Necklace": "a007_b",
    "Vest": "a010_b"
}


def generate_images(traits):
    with ProcessPoolExecutor() as executor:
        executor.map(create_image, traits)


def create_image(nft_image):
    im1 = Image.open(f'./image_data/backgrounds/{backgroundfiles[nft_image["Background"]]}.png').convert('RGBA')
    im2 = Image.open(f'./image_data/bodies/{bodyfiles[nft_image["Body"]]}.png').convert('RGBA')
    im3 = Image.open(f'./image_data/mouths/{mouthfiles[nft_image["Mouth"]]}.png').convert('RGBA')
    im4 = Image.open(f'./image_data/eyes/{eyefiles[nft_image["Eyes"]]}.png').convert('RGBA')
    im5 = Image.open(f'./image_data/heads/{heads[nft_image["Headwear"]]}.png').convert('RGBA')
    im_h_bg = Image.open(f'./image_data/heads/{head_backs[nft_image["Headwear"]]}.png').convert('RGBA')
    im_ac = Image.open(f'./image_data/accessory/{accessories[nft_image["Accessories"]]}.png').convert('RGBA')
    im_ac_bg = Image.open(f'./image_data/accessory/{accessories_backs[nft_image["Accessories"]]}.png').convert('RGBA')

    com0 = Image.alpha_composite(im_ac_bg, im_h_bg)
    com1 = Image.alpha_composite(com0, im2)
    com2 = Image.alpha_composite(com1, im3)
    com3 = Image.alpha_composite(com2, im4)
    com4 = Image.alpha_composite(com3, im_ac)
    com5 = Image.alpha_composite(com4, im5)

    rgb_im = com5.convert('RGB')
    rgb_im = rgb_im.resize((3000, 3000))
    file_name_character_only = str(nft_image["tokenId"]) + "_character_only.png"
    rgb_im.save("./output/character_only/" + file_name_character_only)
    # Generates character only in case you want to use it for marketing or other graphics use

    com_final = Image.alpha_composite(im1, com5)

    rgb_im = com_final.convert('RGB')
    rgb_im = rgb_im.resize((3000, 3000))

    file_name = str(nft_image["tokenId"]) + ".jpg"
    rgb_im.save("./output/" + file_name)  # Saves full size render of image for NFT

    rgb_im_thumb = rgb_im.resize((600, 600))
    file_name = str(nft_image["tokenId"]) + ".jpg"
    rgb_im_thumb.save("./output/thumbnails/" + file_name)  # Saves lower resolution render of image for NFT

    print(f'Image created for Id: {nft_image["tokenId"]}')  # Keeps track of where we are. As we are using a
    #                                                       # processPoolExecutor, it is expected that the image created
    #                                                       # will be out of order during generation process (ie,
    #                                                       # creates #3 ahead of #1, etc).

