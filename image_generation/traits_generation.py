import random
import json

TOTAL_NFT_CharacterS = 10000  # Number of images you want to generate

traits = []

backgrounds = ["White", "Purple", "Black"]

backgroundweights = [50, 30, 20]  # Must add up to 100

bodies = ["Big Chest", "Metal Plates", "Pancakes"]

bodyweights = [50, 30, 20]  # Each section should have different weights

mouths = ["No Teeth", "Smile", "Sad"]

mouthweights = [50, 30, 20]  # All are the same for sake of example/filler

eyes = ["Surprised", "Happy", "Squint"]

eyesweights = [50, 30, 20]

head = ["Normal", "Other", "Test"]

headweights = [50, 30, 20]


accessories = ["None", "Necklace", "Vest"]

accessoryweights = [50, 30, 20]


def create_combo():
    trait = {"Background": random.choices(backgrounds, backgroundweights)[0],
             "Body": random.choices(bodies, bodyweights)[0], "Mouth": random.choices(mouths, mouthweights)[0],
             "Eyes": random.choices(eyes, eyesweights)[0], "Headwear": random.choices(head, headweights)[0],
             "Accessories": random.choices(accessories, accessoryweights)[0]}

    if trait in traits:
        #         if this NFT_Character was already in the Traits list, restart the function
        return create_combo()
    else:
        return trait


## ARE ALL NFT_CharacterS UNIQUE?
def all_unique(x):
    seen = list()
    return not any(i in seen or seen.append(i) for i in x)


def format_for_nft_metadata(generated_traits):
    for trait in generated_traits:
        trait_types = ["Background", "Body", "Eyes", "Mouth", "Headwear", "Accessories"]
        trait_list = []
        for trait_type in trait_types:
            trait_list.append({"trait_type": f"{trait_type}", "value": f"{trait[trait_type]}"})

        ipfs_ready_trait = {"image": f"replaceTextHereWithIPFSResourceCID/{trait['tokenId']}.jpg",
                            "tokenId": f"{trait['tokenId']}",
                            "name": f"NFT_Characters #{trait['tokenId']}",
                            "attributes": trait_list
                            }
        with open(f'../traits/metadata_format/{trait["tokenId"]}', 'w') as outfile:
            json.dump(ipfs_ready_trait, outfile, indent=4)


def generate_all_traits():
    for i in range(TOTAL_NFT_CharacterS):
        newtraitcombo = create_combo()
        traits.append(newtraitcombo)


    # ADD TOKEN IDS TO JSON
    i = 0
    for item in traits:
        item["tokenId"] = i
        i = i + 1

    # GET TRAIT COUNTS

    background_counts = {}
    for item in backgrounds:
        background_counts[item] = 0

    body_counts = {}
    for item in bodies:
        body_counts[item] = 0

    mouth_counts = {}
    for item in mouths:
        mouth_counts[item] = 0

    eyes_counts = {}
    for item in eyes:
        eyes_counts[item] = 0

    head_counts = {}
    for item in head:
        head_counts[item] = 0

    acc_counts = {}
    for item in accessories:
        acc_counts[item] = 0

    for nFT_Character in traits:
        background_counts[nFT_Character["Background"]] += 1
        body_counts[nFT_Character["Body"]] += 1
        mouth_counts[nFT_Character["Mouth"]] += 1
        eyes_counts[nFT_Character["Eyes"]] += 1
        head_counts[nFT_Character["Headwear"]] += 1
        acc_counts[nFT_Character["Accessories"]] += 1

    print(background_counts)
    print(body_counts)
    print(mouth_counts)
    print(eyes_counts)
    print(head_counts)
    print(acc_counts)

    with open('../traits/traits_only.json', 'w') as outfile:
        json.dump(traits, outfile, indent=4)

    return traits


if __name__ == "__main__":
    format_for_nft_metadata(generate_all_traits())
